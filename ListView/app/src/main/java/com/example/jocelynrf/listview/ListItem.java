package com.example.jocelynrf.listview;

/**
 * Created by jocelynrf on 27/02/2017.
 */

public class ListItem {
    int id;
    String url;

    public ListItem(int id, String url) {
        this.id = id;
        this.url = url;
    }

    public String toString() {
        return url;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }
}
