package com.example.jocelynrf.listview;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class REST extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView mListView;
    ArrayList<ListItem> results = new ArrayList<ListItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest);
        mListView = (ListView) findViewById(R.id.listId);
        mListView.setTextFilterEnabled(true);
        mListView.setOnItemClickListener(this);
    }

    public void callWS(View w) {
        new RestCall().execute("452.43.73.158", "8080", "/test");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ListItem item = (ListItem) mListView.getItemAtPosition(position);
        Uri uri = Uri.parse(item.getUrl());
    }

    private class RestCall extends AsyncTask<String, Void, JSONObject> {
        protected JSONObject doInBackground(String... params) {
            return null;
        }

        protected void onPostExecute(JSONObject result) {  }
    }

    public static String converse(String host, int port, String path) throws IOException {
        URL url = new URL("http", host, port, path);
        URLConnection conn = url.openConnection();
        // This does a GET; to do a POST, add conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setAllowUserInteraction(true); // useless but harmless

        conn.connect();

        // To do a POST, you'd write to conn.getOutputStream());

        StringBuilder sb = new StringBuilder();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        in.close();
        return sb.toString();
    }

    protected JSONObject doInBackground(String... params) {
        JSONObject jObject = null;
        try {
            jObject = new JSONObject(converse(params[0], Integer.parseInt(params[1]), params[2]));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObject;

    }

    protected void onPostExecute(JSONObject result) {
        try {
            JSONArray jObject = result.getJSONArray("urls");

            for (int i = 0; i < jObject.length(); i++)
            {
                try {
                    String url = jObject.getString(i);
                    results.add(new ListItem(i, url));
                    System.out.println(url);
                } catch (JSONException e) {
                    // Oops
                    System.out.println("Failed to parse object");
                }
            }
            mListView.setAdapter(new ArrayAdapter<ListItem>(REST.this, R.layout.list_item, results));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
