package com.example.jocelynrf.hw_menu;

        import android.app.Activity;
        import android.os.Bundle;
        import android.view.ContextMenu;
        import android.view.ContextMenu.ContextMenuInfo;
        import android.view.Menu;
        import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.Button;
        import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Button contextButton = (Button) findViewById(R.id.button1);

        //registerForContextMenu(contextButton);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_2, menu);
        //getMenuInflater().inflate(R.menu.menu_2, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {

            case R.id.item_option1:
                Toast.makeText(getApplicationContext(), item.toString(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.item_option2:
                Toast.makeText(getApplicationContext(), item.toString(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.item_option3:
                Toast.makeText(getApplicationContext(), item.toString(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.item_option4:
                Toast.makeText(getApplicationContext(), item.toString(), Toast.LENGTH_SHORT).show();
                break;
            /*case R.id.item1:
                Toast.makeText(getApplicationContext(), "option1 selected", Toast.LENGTH_SHORT).show();

                break;

            case R.id.item2:
                Toast.makeText(getApplicationContext(), "option2 selected", Toast.LENGTH_SHORT).show();

                break;

            case R.id.item3:
                Toast.makeText(getApplicationContext(), "option3 selected", Toast.LENGTH_SHORT).show();

                break;

            case R.id.item4:
                Toast.makeText(getApplicationContext(), "option4 selected", Toast.LENGTH_SHORT).show();

                break;*/
        }

        return super.onOptionsItemSelected(item);
    }

    /*@Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {

        getMenuInflater().inflate(R.menu.contextitems, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }*/


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.item5:
                Toast.makeText(getApplicationContext(), "option5 selected", Toast.LENGTH_SHORT).show();

                break;

            case R.id.item6:
                Toast.makeText(getApplicationContext(), "option6 selected", Toast.LENGTH_SHORT).show();

                break;

            case R.id.item7:
                Toast.makeText(getApplicationContext(), "option7 selected", Toast.LENGTH_SHORT).show();

                break;

            case R.id.item8:
                Toast.makeText(getApplicationContext(), "option8 selected", Toast.LENGTH_SHORT).show();

                break;
        }
        return super.onContextItemSelected(item);
    }

}